from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    result = []
    for i in range(n):
        result.append((a*i+b)%n)
    return result
    


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    perm = compute_permutation(a, b, n)
    result = [0 for i in range(n)]

    for i in range(n):
        result[perm[i]] = i
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    # Convertir le message en une liste d'entiers représentant les codes Unicode des caractères
    unicodes = str_to_unicodes(msg)

    # Calculer la permutation en utilisant la fonction compute_permutation
    permutation = compute_permutation(a, b, 0x110000)


    # Appliquer la permutation aux codes Unicode des caractères
    encrypted_unicodes = []
    for code in unicodes:
        encrypted_unicodes.append(permutation[code] )

    # Convertir les codes Unicode chiffrés en une chaîne de caractères
    encrypted_msg = unicodes_to_str(encrypted_unicodes)

    return encrypted_msg

def encrypt_optimized(msg: str, a: int, b: int) -> str:

    def modulo_inverse(a, n):
        # Calcule l'inverse modulaire de a modulo n
        for x in range(1, n):
            if (a * x) % n == 1:
                return x
        return None

    # Convertir le message en une liste d'entiers représentant les codes Unicode des caractères
    unicodes = str_to_unicodes(msg)

    # Taille de l'alphabet
    n = 0x110000

    # Calcul de l'inverse modulaire de a modulo n
    a_inverse = modulo_inverse(a, n)
    if a_inverse is None:
        raise ValueError("Impossible d'effectuer le chiffrement. 'a' n'est pas inversible modulo la taille de l'alphabet.")

    encrypted_unicodes = []

    for code in unicodes:
        # Application de la formule de chiffrement affine optimisée
        encrypted_code = (a * code + b) % n
        encrypted_unicodes.append(encrypted_code)

    # Convertir les codes Unicode chiffrés en une chaîne de caractères
    encrypted_msg = unicodes_to_str(encrypted_unicodes)

    return encrypted_msg



def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    # Convertir le message en une liste d'entiers représentant les codes Unicode des caractères
    unicodes = str_to_unicodes(msg)

    # Calculer la permutation en utilisant la fonction compute_permutation
    permutation = compute_inverse_permutation(a, b, 0x110000)


    # Appliquer la permutation aux codes Unicode des caractères
    encrypted_unicodes = []
    for code in unicodes:
        encrypted_unicodes.append(permutation[code] )

    # Convertir les codes Unicode chiffrés en une chaîne de caractères
    encrypted_msg = unicodes_to_str(encrypted_unicodes)

    return encrypted_msg

def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # Convertir le message en une liste d'entiers représentant les codes Unicode des caractères
    unicodes = str_to_unicodes(msg)

    # Taille de l'alphabet
    n = 0x110000

    decrypted_unicodes = [(a_inverse * (code - b)) % n for code in unicodes]

    # Convertir les codes Unicode déchiffrés en une chaîne de caractères
    decrypted_msg = unicodes_to_str(decrypted_unicodes)

    return decrypted_msg




def compute_affine_keys(n: int) -> list[int]:
    def gcd(a, b):
        # euclide algo
        while b != 0:
            a, b = b, a % b
        return a

    affine_keys = []
    for a in range(1, n):
        if gcd(a, n) == 1:
            affine_keys.append(a)

    return affine_keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1

    raise RuntimeError(f"{a} N'a pas d'inverse")


def attack() -> tuple[str, tuple[int, int]]:
    def decrypt_brute_force(s, a, b):
        # Déchiffre le message `s` en utilisant la clé `a` et le paramètre `b`
        decrypted_msg = decrypt_optimized(s, a, b)
        return decrypted_msg

    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    keyword = "bombe"
    target_b = 58

    keys = compute_affine_keys(0x110000)

    for a in keys:
        a_inverse = compute_affine_key_inverse(a, keys, 0x110000)
        decrypted_msg = decrypt_brute_force(s, a_inverse, target_b)
        if keyword in decrypted_msg:
            return decrypted_msg, (a_inverse, target_b)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
   
    msg = ""
    possibilites = compute_affine_keys(0x110000)
    for a in range(1,len(possibilites)):
        try:
            a_inverse = compute_affine_key_inverse(a, possibilites, 0x110000)
        except RuntimeError:
            continue
        for b in range(1,15000):
            msg = decrypt_optimized(s, a_inverse, b)
            if "bombe" in msg:
                return (msg, (a, b))

    raise RuntimeError("Failed to attack")
